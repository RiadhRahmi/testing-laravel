<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;
use App\Patterns\Structural\Composite\Form;
use App\Patterns\Structural\Composite\TextElement;
use App\Patterns\Structural\Composite\InputElement;

class CompositeTest extends TestCase
{
    public function testRender()
    {
        $form = new Form();
        $form->addElement(new TextElement('Email:'));
        $form->addElement(new InputElement());
        $form->addElement(new TextElement('Password:'));
        $form->addElement(new InputElement());

        // This is just an example, in a real world scenario it is important to remember that web browsers do not
        // currently support nested forms

        $this->assertSame(
            '<form>Email:<input type="text" />Password:<input type="text" /></form>',
            $form->render()
        );
    }
}