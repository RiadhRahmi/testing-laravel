<?php

namespace Tests\Unit;

use App\Patterns\Creational\AbstractFactory\BenzCar;
use App\Patterns\Creational\AbstractFactory\BMWCar;
use App\Patterns\Creational\AbstractFactory\CarAbstractFactory;
use App\Patterns\Creational\AbstractFactory\CsvWriter;
use App\Patterns\Creational\AbstractFactory\JsonWriter;
use App\Patterns\Creational\AbstractFactory\UnixWriterFactory;
use App\Patterns\Creational\AbstractFactory\WinWriterFactory;
use App\Patterns\Creational\AbstractFactory\WriterFactory;
use PHPUnit\Framework\TestCase;

class AbstractFactoryTest extends TestCase
{

    /**
     * @return void
     */
    public  function testCanCreateBMWCare()
    {
        $careFactory = new CarAbstractFactory(200000);
        $myCar = $careFactory->createBMWCare();

        $this->assertInstanceOf(BMWCar::class,$myCar);
    }

    /**
     * @return void
     */
    public  function testCanCreateBenzCar(){

        $carFactory = new CarAbstractFactory(200000);
        $myCar = $carFactory->createBenzCar();

        $this->assertInstanceOf(BenzCar::class ,$myCar);
    }

    public function provideFactory()
    {
        return [
            [new UnixWriterFactory()],
            [new WinWriterFactory()]
        ];
    }

    /**
     * @dataProvider provideFactory
     */
    public function testCanCreateCsvWriterOnUnix(WriterFactory $writerFactory)
    {
        $this->assertInstanceOf(JsonWriter::class, $writerFactory->createJsonWriter());
        $this->assertInstanceOf(CsvWriter::class, $writerFactory->createCsvWriter());
    }
}
