<?php

namespace Tests\Unit;


use PHPUnit\Framework\TestCase;
use App\Patterns\Structural\Bridge\HtmlFormatter;
use App\Patterns\Structural\Bridge\HelloWorldService;
use App\Patterns\Structural\Bridge\PlainTextFormatter;


class BridgeTest extends TestCase
{
    public function testCanPrintUsingThePlainTextFormatter()
    {
        $service = new HelloWorldService(new PlainTextFormatter());

        $this->assertSame('Hello World', $service->get());
    }

    public function testCanPrintUsingTheHtmlFormatter()
    {
        $service = new HelloWorldService(new HtmlFormatter());

        $this->assertSame('<p>Hello World</p>', $service->get());
    }
}