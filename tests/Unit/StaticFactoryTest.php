<?php

namespace Tests\Unit;

use App\Patterns\Creational\StaticFactory\FormatNumber;
use App\Patterns\Creational\StaticFactory\FormatString;
use App\Patterns\Creational\StaticFactory\StaticFactory;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;


class StaticFactoryTest extends TestCase
{
    public function testCanCreateNumberFormatter()
    {
        $this->assertInstanceOf(FormatNumber::class, StaticFactory::factory('number'));
    }

    public function testCanCreateStringFormatter()
    {
        $this->assertInstanceOf(FormatString::class, StaticFactory::factory('string'));
    }

    public function testException()
    {
        $this->expectException(InvalidArgumentException::class);

        StaticFactory::factory('object');
    }

    public function testExceptioner()
    {
        $staticFactory = new StaticFactory();
        $this->assertInstanceOf(FormatString::class, $staticFactory->factory('string'));
    }
}
