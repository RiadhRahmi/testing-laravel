<?php

namespace Tests\Unit;


use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use App\Patterns\Structural\DataMapper\User;
use App\Patterns\Structural\DataMapper\UserMapper;
use App\Patterns\Structural\DataMapper\StorageAdapter;


class DataMapperTest extends TestCase
{
    public function testCanMapUserFromStorage()
    {
        $storage = new StorageAdapter([1 => ['username' => 'domnikl', 'email' => 'liebler.dominik@gmail.com']]);
        $mapper = new UserMapper($storage);

        $user = $mapper->findById(1);

        $this->assertInstanceOf(User::class, $user);
    }

    public function testWillNotMapInvalidData()
    {
        $this->expectException(InvalidArgumentException::class);

        $storage = new StorageAdapter([]);
        $mapper = new UserMapper($storage);

        $mapper->findById(1);
    }
}
