<?php

namespace Tests\Unit;


use App\Patterns\Creational\Builder\BmwBusBuilder;
use App\Patterns\Creational\Builder\KiaCarBuilder;
use App\Patterns\Creational\Builder\Vehicle;
use App\Patterns\Creational\Builder\VehicleDirector;
use PHPUnit\Framework\TestCase;


class BuilderTest extends TestCase
{
    public function test_builder_pattern()
    {
        $kiaCar = (new VehicleDirector())->build(new KiaCarBuilder(new Vehicle()));
        $bmwBus = (new VehicleDirector())->build(new BmwBusBuilder(new Vehicle()));

        $this->assertInstanceOf(Vehicle::class, $kiaCar);
        $this->assertInstanceOf(Vehicle::class, $bmwBus);
    }
}
