<?php

namespace Tests\Unit;


use PHPUnit\Framework\TestCase;
use App\Patterns\Structural\DependencyInjection\DatabaseConnection;
use App\Patterns\Structural\DependencyInjection\DatabaseConfiguration;


class DependencyInjectionTest extends TestCase
{
    public function testDependencyInjection()
    {
        $config = new DatabaseConfiguration('localhost', 3306, 'domnikl', '1234');
        $connection = new DatabaseConnection($config);

        $this->assertSame('domnikl:1234@localhost:3306', $connection->getDsn());
    }
}