<?php


namespace Tests\Unit;

use PHPUnit\Framework\TestCase;
use App\Patterns\Behavioral\ChainOfResponsibilities\Request;
use App\Patterns\Behavioral\ChainOfResponsibilities\AliHandler;
use App\Patterns\Behavioral\ChainOfResponsibilities\AfafHandler;
use App\Patterns\Behavioral\ChainOfResponsibilities\MohsenHandler;

class ChainOfResponsibilityTest extends TestCase
{
    public function testAliCanHandlerRequest()
    {
        $ali = new AliHandler();
        $afaf = new AfafHandler();
        $mohsen = new MohsenHandler();
        $ali->setNext($afaf->setNext($mohsen));
        $request = new Request();
        $request->setId(4);
        /**@var Request $reponse
         */
        $response =  $ali->handle($request);

        self::assertTrue($response->isDone());
        self::assertEquals(AliHandler::class,$response->getHandler());
    }

    public function testAFAFCanHandlerRequest()
    {
        $ali = new AliHandler();
        $afaf = new AfafHandler();
        $mohsen = new MohsenHandler();

        $afaf->setNext($mohsen);
        $ali->setNext($afaf);
        $request = new Request();
        $request->setId(9);
        /**@var Request $reponse
         */
        $response =  $ali->handle($request);

        self::assertTrue($response->isDone());
        self::assertEquals(AfafHandler::class,$response->getHandler());
    }

    public function testMohsenCanHandlerRequest()
    {
        $ali = new AliHandler();
        $afaf = new AfafHandler();
        $mohsen = new MohsenHandler();

        $afaf->setNext($mohsen);
        $ali->setNext($afaf);
        $request = new Request();
        $request->setId(27);
        /**@var Request $reponse
         */
        $response =  $ali->handle($request);

        self::assertTrue($response->isDone());
        self::assertEquals(MohsenHandler::class,$response->getHandler());
    }

    public function testNOoneCanHandlerRequest()
    {
        $ali = new AliHandler();
        $afaf = new AfafHandler();
        $mohsen = new MohsenHandler();

        $afaf->setNext($mohsen);
        $ali->setNext($afaf);
        $request = new Request();
        $request->setId(71);
        /**@var Request $reponse
         */
        $response =  $ali->handle($request);

        self::assertFalse($response->isDone());
    }
}