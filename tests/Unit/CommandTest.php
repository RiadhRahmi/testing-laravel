<?php


namespace Tests\Unit;

use PHPUnit\Framework\TestCase;
use App\Patterns\Behavioral\Command\Invoker;
use App\Patterns\Behavioral\Command\Receiver;
use App\Patterns\Behavioral\Command\HelloCommand;

class CommandTest extends TestCase
{
    public function testInvocation()
    {
        $invoker = new Invoker();
        $receiver = new Receiver();

        $invoker->setCommand(new HelloCommand($receiver));
        $invoker->run();
        $this->assertSame('Hello World', $receiver->getOutput());
    }
}