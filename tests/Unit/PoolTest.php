<?php

namespace Tests\Unit;


use App\Patterns\Creational\Pool\Car;
use App\Patterns\Creational\Pool\CarPool;
use App\Patterns\Creational\Pool\WorkerPool;
use PHPUnit\Framework\TestCase;


class PoolTest extends TestCase
{
    private $carPool;

    protected function setUp(): void
    {
        parent::setUp();
        $this->carPool = new CarPool();
    }


    public function testCanRentCar()
    {
        $myCar = $this->carPool->rentCar();
        $this->assertInstanceOf(Car::class, $myCar);
        $this->assertEquals(1, $this->carPool->getReport());
    }

    public function testCanFreeCar()
    {
        $myCar = $this->carPool->rentCar();
        $myCar2 = $this->carPool->rentCar();
        $this->assertEquals(0, $this->carPool->getFreeCount());

        $this->carPool->freeCar($myCar);
        $this->assertEquals(2, $this->carPool->getReport());
        $this->assertEquals(1, $this->carPool->getFreeCount());

    }

    public function testCanGetNewInstancesWithGet()
    {
        $pool = new WorkerPool();
        $worker1 = $pool->get();
        $worker2 = $pool->get();

        $this->assertCount(2, $pool);
        $this->assertNotSame($worker1, $worker2);
    }

    public function testCanGetSameInstanceTwiceWhenDisposingItFirst()
    {
        $pool = new WorkerPool();
        $worker1 = $pool->get();
        $pool->dispose($worker1);
        $worker2 = $pool->get();

        $this->assertCount(1, $pool);
        $this->assertSame($worker1, $worker2);
    }

}
