<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;
use App\Patterns\Structural\Facade\Bios;
use App\Patterns\Structural\Facade\Facade;
use App\Patterns\Structural\Facade\OperatingSystem;

class FacadeTest extends TestCase
{
    public function testComputerOn()
    {
        $os = $this->createMock(OperatingSystem::class);

        $os->method('getName')
            ->will($this->returnValue('Linux'));

        $bios = $this->createMock(Bios::class);

        $bios->method('launch')
            ->with($os);

        $facade = new Facade($bios, $os);
        $facade->turnOn();

        $this->assertSame('Linux', $os->getName());
    }
}