<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;
use App\Patterns\Others\Hydrator\Employee;
use App\Patterns\Others\Hydrator\Converter;

class HydratorTest extends TestCase
{

    public function testConvertArrayToObject()
    {
        $arr['name'] = "Adam";
        $arr['phone'] = "123456";
        $arr['email'] = "adam@mail.com";
        $arr['address'] = "U.S";
        $obj = Converter::toObject($arr, Employee::class);

        $this->assertInstanceOf(Employee::class, $obj);
    }

    public function testConvertOjectToArray()
    {
        $employee = new Employee();
        $employee->setAddress("U.S");
        $employee->setEmail("adam@mail.com");
        $employee->setPhone("123456");
        $employee->setName("Adam");

        $array = Converter::toArray($employee);

        $this->assertIsArray($array);
    }
}
