<?php

namespace Tests\Unit;


use PHPUnit\Framework\TestCase;
use App\Patterns\Structural\Adapter\Kindle;
use App\Patterns\Structural\Adapter\PaperBook;
use App\Patterns\Structural\Adapter\EBookAdapter;


class AdapterTest extends TestCase
{

    public function testCanTurnPageOnBook()
    {
        $book = new PaperBook();
        $book->open();
        $book->turnPage();

        $this->assertSame(2, $book->getPage());
    }

    public function testCanTurnPageOnKindleLikeInANormalBook()
    {
        $kindle = new Kindle();
        $book = new EBookAdapter($kindle);

        $book->open();
        $book->turnPage();

        $this->assertSame(2, $book->getPage());
    }
}
