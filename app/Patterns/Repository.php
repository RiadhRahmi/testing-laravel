<?php

namespace App\Patterns;

class Repository implements RepositoryInterface
{
    public function find($id)
    {
        echo 'user number '.$id;
    }

    public function create(array $data)
    {
        // Create a new user in the database
        // Example: $user = User::create($data);
    }

    public function update($id, array $data)
    {
        // Update a user in the database
        // Example: $user = User::find($id);
        //           $user->update($data);
    }

    public function delete($id)
    {
        // Delete a user from the database
        // Example: $user = User::find($id);
        //           $user->delete();
    }
}

interface RepositoryInterface
{
    public function find($id);

    public function create(array $data);

    public function update($id, array $data);

    public function delete($id);
    // Add more methods as per your requirements
}
