<?php

declare(strict_types=1);

namespace App\Patterns\Structural\Composite;

class InputElement implements Renderable
{
    public function render(): string
    {
        return '<input type="text" />';
    }
}