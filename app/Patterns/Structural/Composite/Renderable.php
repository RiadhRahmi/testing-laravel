<?php

declare(strict_types=1);

namespace App\Patterns\Structural\Composite;

interface Renderable
{
    public function render(): string;
}