<?php

declare(strict_types=1);

namespace App\Patterns\Structural\Adapter;


/**
 * EBookAdapter
 */
class EBookAdapter implements Book
{
    /**
     * __construct
     *
     * @param  mixed $eBook
     * @return void
     */
    public function __construct(protected EBook $eBook)
    {
    }


    /**
     * open
     *
     * @return void
     */
    public function open()
    {
        $this->eBook->unlock();
    }

    /**
     * turnPage
     *
     * @return void
     */
    public function turnPage()
    {
        $this->eBook->pressNext();
    }


    /**
     * getPage
     *
     * @return int
     */
    public function getPage(): int
    {
        return $this->eBook->getPage()[0];
    }
}
