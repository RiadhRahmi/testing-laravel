<?
namespace App\Patterns\Structural\ActiveRecord;

// https://matthiasnoback.nl/2022/08/simple-solutions-1-active-record-versus-data-mapper/

final class User
{
    public function __construct(
        private string $name
    ) {
    }

    public function save(): void
    {
        // get the DB connection

        // $connection->execute(
        //     'INSERT INTO users SET name = ?',
        //     [
        //         $this->name
        //     ]
        // );
    }
}