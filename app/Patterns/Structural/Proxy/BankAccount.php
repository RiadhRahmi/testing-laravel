<?php

declare(strict_types=1);

namespace App\Patterns\Structural\Proxy;

interface BankAccount
{
    public function deposit(int $amount);

    public function getBalance(): int;
}