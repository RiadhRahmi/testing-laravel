<?php

namespace App\Patterns;

abstract class AbstractClass
{
    // Template method
    public function templateMethod()
    {
        $this->step1();
        $this->step2();
        $this->step3();
    }

    // Abstract methods to be overridden by subclasses
    abstract protected function step1();

    abstract protected function step2();

    // Hook method (optional)
    protected function step3()
    {
        echo 'Step 3';
    }
}

class TemplateMethod extends AbstractClass
{
    protected function step1()
    {
        echo 'STEP 1';
    }

    protected function step2()
    {
        echo 'Step 2';
    }
}
