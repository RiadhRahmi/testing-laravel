<?php

namespace App\Patterns\Behavioral\ChainOfResponsibilities;


interface HandlerInterface
{
    public function setNext( HandlerInterface $handler);
    public function handle(Request $request);
}