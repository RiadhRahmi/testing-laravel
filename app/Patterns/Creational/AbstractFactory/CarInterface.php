<?php

namespace App\Patterns\Creational\AbstractFactory;

interface CarInterface
{
    public function calculatePrice();
}
