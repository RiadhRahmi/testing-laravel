<?php

namespace App\Patterns\Creational\AbstractFactory;

class UnixCsvWriter implements CsvWriter
{
    public function write(array $line): string
    {
        return implode(',', $line)."\n";
    }
}
