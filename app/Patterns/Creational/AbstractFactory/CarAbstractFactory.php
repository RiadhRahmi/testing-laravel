<?php

namespace App\Patterns\Creational\AbstractFactory;

/**
 * To create series of related or dependent objects without specifying their concrete classes.
 * Usually the created classes all implement the same interface.
 * The client of the abstract factory does not care about how these objects are created,
 * it just knows how they go together.
 *
 * https://www.youtube.com/watch?v=Z7U9RJyEBR4&list=PLdYYj2XLw5BnpInmR103TyVwFd_CLI6IS&index=2
 * https://designpatternsphp.readthedocs.io/en/latest/Creational/AbstractFactory/README.html#
 */
class CarAbstractFactory
{
    private $tax = 100000;

    private $price;

    public function __construct($price)
    {
        $this->price = $price;
    }

    public function createBMWCare(): BMWCar
    {
        return new BMWCar($this->price);
    }

    public function createBenzCar(): BenzCar
    {
        return new BenzCar($this->price, $this->tax);
    }
}
