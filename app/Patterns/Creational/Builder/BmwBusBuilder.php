<?php

namespace App\Patterns\Creational\Builder;

class BmwBusBuilder implements VehicleBuilderInterface
{
    private $vehicle;

    public function __construct(Vehicle $vechile)
    {
        $this->vehicle = $vechile;
    }

    public function setModel()
    {
        $this->vehicle->model = 'Bmw';
    }

    public function setEnginesCount()
    {
        $this->vehicle->enginesCount = 2;
    }

    public function setType()
    {
        $this->vehicle->type = Vehicle::BUS;
    }

    public function getVehicle()
    {
        return $this->vehicle;
    }
}
