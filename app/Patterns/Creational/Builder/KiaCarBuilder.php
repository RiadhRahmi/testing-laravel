<?php

namespace App\Patterns\Creational\Builder;

class KiaCarBuilder implements VehicleBuilderInterface
{
    private $vehicle;

    public function __construct(Vehicle $vehicle)
    {
        $this->vehicle = $vehicle;
    }

    public function setModel()
    {
        $this->vehicle->model = 'Kia';
    }

    public function setEnginesCount()
    {
        $this->vehicle->enginesCount = 1;
    }

    public function setType()
    {
        $this->vehicle->type = Vehicle::CAR;
    }

    public function getVehicle()
    {
        return $this->vehicle;
    }
}
