<?php

namespace App\Patterns\Creational\Builder;

// Concrete Class
class Vehicle
{
    public $model;

    public $enginesCount;

    public $type;

    const CAR = 'Car';

    const BUS = 'Bus';

    const TRAILER = 'Trailer';

    public function __construct()
    {
    }
}
