<?php

declare(strict_types=1);

namespace App\Patterns\Others\Hydrator;

class Employee
{
    protected $name;
    protected $phone;
    protected $email;
    protected $address;
    public function getName()
    {
        return $this->name;
    }
    public function getPhone()
    {
        return $this->phone;
    }
    public function getEmail()
    {
        return $this->email;
    }
    public function getAddress()
    {
        return $this->address;
    }
    public function setName($name)
    {
        $this->name = $name;
    }
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }
    public function setEmail($email)
    {
        $this->email = $email;
    }
    public function setAddress($address)
    {
        $this->address = $address;
    }
}